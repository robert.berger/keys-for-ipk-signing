=== create git repo ===

cd keys-for-ipk-signing

in case it's not yet a git repo you could do something like this:

git init
git remote add origin git@gitlab.com:robert.berger/keys-for-ipk-signing.git

Do NOT add/commit!

make sure that it already contains the files you want to encrypt
it might also contain files which will stay unencrypted

=== init git-crypt ==

$ git-crypt init
Generating key...

=== gpg show keys ===

gpg --list-secret-keys --keyid-format LONG

from there you should get some e-mail address(es)

=== add git-crypt master key ===

Add a copy of the master key, encrypted with your public GPG key (so that only you can decrypt it)

$ git-crypt add-gpg-user --trusted your.email@domain.com <-- from above

[master bd35041] Add 1 git-crypt collaborator
 2 files changed, 4 insertions(+)
 create mode 100644 .git-crypt/.gitattributes
 create mode 100644 .git-crypt/keys/default/0/6CE0E1E29F569753A679F686F2D9E720D6243937.gpg

=== config ===

$ tree
.
├── backup
│   ├── ipkkeypriv-armor.gpg
│   ├── ipkkeypriv.gpg
│   ├── ipkkeypub-armor.gpg
│   └── ipkkeypub.gpg
├── doc
│   └── readme.txt
└── passphrase
    └── passphrase.txt

3 directories, 6 files

vim .gitattributes

-->

# encrypt backup/ipkkeypriv-armor.gpg
backup/ipkkeypriv-armor.gpg filter=git-crypt diff=git-crypt

# encrypt backup/ipkkeypriv.gpg
backup/ipkkeypriv.gpg filter=git-crypt diff=git-crypt

# encrypt backup/ipkkeypub-armor.gpg
backup/ipkkeypub-armor.gpg filter=git-crypt diff=git-crypt

# encrypt backup/ipkkeypub.gpg
backup/ipkkeypub.gpg filter=git-crypt diff=git-crypt

# encrypt passphrase/passphrase.txt
passphrase/passphrase.txt filter=git-crypt diff=git-crypt

# Making sure that .gitattributes is never encrypted. DON'T TOUCH THAT LINE AND ONE BELOW
.gitattributes !filter !diff

<--


== add don't commit ==

git add .

== check git-crypt status ==

$ git-crypt status
not encrypted: .git-crypt/.gitattributes
not encrypted: .git-crypt/keys/default/0/6CE0E1E29F569753A679F686F2D9E720D6243937.gpg
not encrypted: .gitattributes
    encrypted: backup/ipkkeypriv-armor.gpg
    encrypted: backup/ipkkeypriv.gpg
    encrypted: backup/ipkkeypub-armor.gpg
    encrypted: backup/ipkkeypub.gpg
not encrypted: doc/readme.txt
    encrypted: passphrase/passphrase.txt

=== push upstream ===

git commit -m "Initial commit"
git push -u origin master

=== export git-crypt keys ===

keys-for-ipk-signing

mkdir -p ../git-crypt-keys

git-crypt export-key ../git-crypt-keys/keys-for-ipk-signing-key

=== use exported keys ===

e.g. from another machine

git-crypt unlock /tmp/keys-for-ipk-signing-key
